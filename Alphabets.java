/**
 * @authors Samuel Gu, Frances Antwi-Donkor, Constant Likudie and Deborah Akosua Attuah
 * This class prints an enhance version of letters of any word entered by a user on the console
 **/
import java.util.Scanner;
public class Alphabets {
     public static void main (String [] args){
          Alphabets alpha = new Alphabets();
          Scanner keyboard = new Scanner(System.in);
          System.out.println("Please enter your word");
          alpha.printUserWord(keyboard.nextLine());
          
     }
     /**
     *This method prints the letter A
     */
     
     public void printA(){
  System.out.println("          a           ");
  System.out.println("        a   a         ");
  System.out.println("       a     a        ");
  System.out.println("      a       a       ");
  System.out.println("     a         a      ");
  System.out.println("    a a a a a a a     ");
  System.out.println("   a             a    ");
  System.out.println("  a               a   ");
  System.out.println(" a                 a  ");
  System.out.println("a                   a");}
     
     /**
     *This method prints the letter B
     */
     public void printB(){
  System.out.println("  b b b b b b       ");
  System.out.println("  b           b     ");
  System.out.println("  b            b    ");
  System.out.println("  b           b     ");
  System.out.println("  b b b b b b       "); 
  System.out.println("  b           b     ");
  System.out.println("  b            b    ");
  System.out.println("  b            b    ");
  System.out.println("  b           b     ");
  System.out.println("  b b b b b b       ");}
    
    /**
     *This method prints the letter C
     */ 
     public void printC(){
  System.out.println("      c c c         ");
  System.out.println("    c       c       ");
  System.out.println("   c                 ");
  System.out.println("   c                 ");
  System.out.println("   c                 "); 
  System.out.println("   c                 ");
  System.out.println("   c                 ");
  System.out.println("   c                 ");
  System.out.println("    c       c        ");
  System.out.println("      c c c         ");}

  /**
     *This method prints the letter D
     */
     public void printD(){
  System.out.println("   d d d d          ");
  System.out.println("   d        d       ");
  System.out.println("   d         d      ");
  System.out.println("   d          d     ");
  System.out.println("   d          d     "); 
  System.out.println("   d          d     ");
  System.out.println("   d          d     ");
  System.out.println("   d         d      ");
  System.out.println("   d        d       ");
  System.out.println("   d d d d          ");}

          /**
     *This method prints the letter G
     */
     public void printG(){
  System.out.println("     g g g g          ");
  System.out.println("    g        g        ");
  System.out.println("   g         g        ");
  System.out.println("   g                  ");
  System.out.println("   g                  "); 
  System.out.println("   g      g g g g     ");
  System.out.println("   g         g        ");
  System.out.println("   g         g        ");
  System.out.println("    g        g        ");
  System.out.println("     g g g g          ");}


  /**
     *This method prints the letter E
     */
     public void printE(){
  System.out.println("  e e e e e e e      ");
  System.out.println("  e                  ");
  System.out.println("  e                  ");
  System.out.println("  e                  ");
  System.out.println("  e e e e e e        "); 
  System.out.println("  e                  ");
  System.out.println("  e                  ");
  System.out.println("  e                  ");
  System.out.println("  e                  ");
  System.out.println("  e e e e e e e      ");}

     /**
     *This method prints the letter F
     */
     public void printF(){
  System.out.println("  f f f f f f f      ");
  System.out.println("  f                  ");
  System.out.println("  f                  ");
  System.out.println("  f                  ");
  System.out.println("  f f f f            "); 
  System.out.println("  f                  ");
  System.out.println("  f                  ");
  System.out.println("  f                  ");
  System.out.println("  f                  ");
  System.out.println("  f                  ");}



     
     /**
     *This method prints the letter H
     */
     public void printH(){
          for (int i = 0; i < 4; i++)
               System.out.println("hhh   hhh");
          for (int i = 4; i < 6; i++)
               System.out.println("hhhhhhhhh");
          for (int i = 6; i < 10; i++)
               System.out.println("hhh   hhh");
          System.out.println();
     }
     
     /**
     *This method prints the letter I
     */
     public void printI(){
          for (int i = 0; i < 2; i++)
               System.out.println("iiiiiiii");
          for (int i = 2; i < 8; i++)
               System.out.println("  iii   ");
          for (int i = 8; i < 10; i++)
               System.out.println("iiiiiiii");
          System.out.println();
          
     }
     
     /**
     *This method prints the letter J
     */
     public void printJ(){
          for (int i = 0; i < 2; i++)
               System.out.println("jjjjjjjjj");
          for (int i = 2; i < 8; i++)
               System.out.println("   jjj   ");
          for (int i = 8; i < 9; i++)
               System.out.println("jjjjj");
          for (int i = 9; i < 10; i++)
               System.out.println("jjjj");
          System.out.println();
     }
     
     /**
     *This method prints the letter K
     */
     public void printK(){
          System.out.println("kkk    kkk");
          System.out.println("kkk   kkk");
          System.out.println("kkk  kkk");
          System.out.println("kkk kkk");
          System.out.println("kkkkkk");
          System.out.println("kkkkkk");
          System.out.println("kkk kkk");
          System.out.println("kkk  kkk");
          System.out.println("kkk   kkk");
          System.out.println("kkk    kkk");
          System.out.println();
     }

     /**
     *This method prints the letter L
     */
     public void printL(){
          for (int i = 0; i < 8; i++)
               System.out.println("llll");
          for (int i = 8; i < 10; i++)
               System.out.println("llllllllll");
          System.out.println();
     }
     
     /**
     *This method prints the letter M
     */
     public void printM(){
    System.out.println("m               m");
    System.out.println("mm             mm");
    System.out.println("m m           m m");
    System.out.println("m  m         m  m");
    System.out.println("m   m       m   m");
    System.out.println("m    m     m    m");
    System.out.println("m     m   m     m");
    System.out.println("m      m m      m");
    System.out.println("m       m       m");
    System.out.println("m               m");

  
}

    
    

/**
     *This method prints the letter N
     */
     public void printN(){
     
    System.out.println("nn        n");
    System.out.println("n n       n");
    System.out.println("n  n      n");
    System.out.println("n   n     n");
    System.out.println("n    n    n");
    System.out.println("n     n   n");
    System.out.println("n      n  n");
    System.out.println("n       n n");
    System.out.println("n        nn");
    System.out.println("n         n");
}

     /**
     *This method prints the letter O
     */
     public void printO(){
    System.out.println("     o o o     ");
    System.out.println("  o         o  ");
    System.out.println(" o           o ");
    System.out.println("o             o");
    System.out.println("o             o");
    System.out.println("o             o");
    System.out.println("o             o");
    System.out.println(" o           o ");
    System.out.println("  o         o  ");
    System.out.println("     o o o     ");
     }

     /**
     *This method prints the letter P
     */
     public void printP(){
     System.out.println("p p p pp");
    System.out.println("p       p");
    System.out.println("p        p");
    System.out.println("p        p");
    System.out.println("p       p");
    System.out.println("p p p pp");
    System.out.println("p");
    System.out.println("p");
    System.out.println("p");
    System.out.println("p");
     }

     /**
     *This method prints the letter Q
     */
     public void printQ(){
    System.out.println("     q q q     ");
    System.out.println("  q         q  ");
    System.out.println(" q           q ");
    System.out.println("q             q ");
    System.out.println("q             q");
    System.out.println("q             q");
    System.out.println("q             q ");
    System.out.println(" q       q   q  ");
    System.out.println("   q q q q q  ");
    System.out.println("             q  ");
     }

     /**
     *This method prints the letter R
     */
     public void printR(){
     System.out.println("r r r rr");
    System.out.println("r        r");
    System.out.println("r        r");
    System.out.println("r        r");
    System.out.println("r      r");
    System.out.println("r r r r");
    System.out.println("r      r");
    System.out.println("r       r");
    System.out.println("r        r");
    System.out.println("r         r");
     }

     /**
     *This method prints the letter S
     */
     public void printS(){
     System.out.println("     ss     ");
    System.out.println("  s      s  ");
    System.out.println("  s         ");
    System.out.println("   s        ");
    System.out.println("     s      ");
    System.out.println("        s    ");
    System.out.println("          s  ");
    System.out.println("          s   ");
    System.out.println("  s      s    ");
    System.out.println("     ss       ");
     }

     /**
     *This method prints the letter T
     */
     public void printT(){
          System.out.println("ttttttttttttttttt");
          System.out.println("        tt       ");
          System.out.println("        tt       ");
          System.out.println("        tt       ");
          System.out.println("        tt       ");
          System.out.println("        tt       ");
          System.out.println("        tt       ");
          System.out.println("        tt       ");
          System.out.println("        tt       ");
          System.out.println("        tt       ");
     }

     /**
     *This method prints the letter U
     */
     public void printU(){
          System.out.println("u               u");
          System.out.println("u               u");
          System.out.println("u               u");
          System.out.println("u               u");
          System.out.println("u               u");
          System.out.println("u               u");
          System.out.println("u               u");
          System.out.println("u               u");
          System.out.println(" u             u");
          System.out.println("  uuuuuuuuuuuu");
     }

     /**
     *This method prints the letter V
     */
     public void printV(){
          System.out.println("v                 v");
          System.out.println(" v               v");
          System.out.println("  v             v");
          System.out.println("   v           v");
          System.out.println("    v         v");
          System.out.println("     v       v");
          System.out.println("      v     v");
          System.out.println("       v   v");
          System.out.println("        v v      ");
          System.out.println("         v");
     }

     /**
     *This method prints the letter W
     */
     public void printW(){
          System.out.println("ww                ww");
          System.out.println("ww                ww");
          System.out.println("ww                ww");
          System.out.println("ww                ww");
          System.out.println("ww                ww");
          System.out.println("ww       ww       ww");
          System.out.println("ww     w    w     ww");
          System.out.println("ww   w        w   ww");
          System.out.println("ww  w           w ww");
          System.out.println("www               ww");
     }

     /**
     *This method prints the letter X
     */
     public void printX(){
          System.out.println("xxx              xxx");
          System.out.println("  xxx           xxx ");
          System.out.println("    xxx       xxx   ");
          System.out.println("     xxx    xxx     ");
          System.out.println("       xxxxxxx      ");
          System.out.println("        xxxxx        ");
          System.out.println("       xxxxxxx       ");
          System.out.println("     xxx      xxx       ");
          System.out.println("   xxx          xxx      ");
          System.out.println(" xxx              xxx");
          System.out.println("xxx                 xxx");
     }

     /**
     *This method prints the letter Y
     */
     public void printY(){
          System.out.println("yy             yy");
          System.out.println("  yy         yy ");
          System.out.println("    yy    yy     ");
          System.out.println("       yy        ");
          System.out.println("       yy        ");
          System.out.println("       yy        ");
          System.out.println("       yy        ");
          System.out.println("       yy        ");
          System.out.println("       yy        ");
          System.out.println("       yy        ");
     }

     /**
     *This method prints the letter Z
     */
     public void printZ(){
          System.out.println("zzzzzzzzzzzzzzzzz");
          System.out.println("                zz");
          System.out.println("              zz ");
          System.out.println("            zz   ");
          System.out.println("          zz     ");
          System.out.println("       zz        ");
          System.out.println("     zz          ");
          System.out.println("  zz             ");
          System.out.println("zz               ");
          System.out.println("zzzzzzzzzzzzzzzzz");
     }

     /**
     *This method prints space 
     */
     public void printSpace(){
          for (int i = 0; i < 10; i++)
               System.out.println("           ");
     }
     
     /**
     *This method does the actual printing of the individual letters of a word on the console
     */
     public void printUserWord(String word){
          for (int i = 0; i < word.length(); i++){
               char userChar = word.charAt(i);
               
               switch (userChar){
                    case 'a':
                         printA();
                         break;
                    case 'b':
                         printB();
                         break;
                    case 'c':
                         printC();
                         break;
                    case 'd':
                         printD();
                         break;
                    case 'e':
                         printE();
                         break;
                    case 'f':
                         printF();
                         break;
                    case 'g':
                         printG();
                         break;
                    case 'h':
                         printH();
                         break;
                    case 'i':
                         printI();
                         break;
                    case 'j':
                         printJ();
                         break;
                    case 'k':
                         printK();
                         break;
                    case 'l':
                         printL();
                         break;
                    case 'm':
                         printM();
                         break;
                    case 'n':
                         printN();
                         break;
                    case 'o':
                         printO();
                         break;
                    case 'p':
                         printP();
                         break;
                    case 'q':
                         printQ();
                         break;
                    case 'r':
                         printR();
                         break;
                    case 's':
                         printS();
                         break;
                    case 't':
                         printT();
                         break;
                    case 'u':
                         printU();
                         break;
                    case 'v':
                         printV();
                         break;
                    case 'w':
                         printW();
                         break;
                    case 'x':
                         printX();
                         break;
                    case 'y':
                         printY();
                         break;
                    case 'z':
                         printZ();
                         break;
                    case ' ':
                         printSpace();
                         break;
                    default:
                         break;
               }
          }
     }
     
}
